﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIioasys.Context;
using APIioasys.Models;
using Microsoft.AspNetCore.JsonPatch;
using PagedList;

namespace APIioasys.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly DBContext _context;

        public UsuariosController(DBContext context)
        {
            _context = context;
        }


        // GET: api/Usuarios
        [HttpGet]
        public async Task<List<Usuarios>> GetUsuarios(int idSolicitante, int? pagina)
        {
            int pageSize = 10;
            int pageNumber = pagina ?? 1;

            var usuarioDB = await _context.Usuarios.FirstOrDefaultAsync(u => u.Id == idSolicitante);

            if (usuarioDB != null && usuarioDB.PerfilId == 2)
            {
                var lstUser = _context.Usuarios.Where(u => u.PerfilId == 1 && u.Ativo == true).ToList();
                return lstUser.OrderBy(n=> n.Nome).ToPagedList(pageNumber, pageSize).ToList(); 
            }
            return null;
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult> DesativarUsuario(int id, JsonPatchDocument<Usuarios> usuario)
        {
            usuario.Operations[0].path = "/Ativo";
            usuario.Operations[0].op = "replace";
            usuario.Operations[0].value = "false";

            if (usuario == null)
            {
                return BadRequest();
            }

            var usuarioDB = await _context.Usuarios.FirstOrDefaultAsync(x => x.Id == id);

            if (usuarioDB == null)
            {
                return NotFound();
            }

            usuario.ApplyTo(usuarioDB, ModelState);

            var isValid = TryValidateModel(usuarioDB);

            if (!isValid)
            {
                return BadRequest(ModelState);
            }

            await _context.SaveChangesAsync();

            return NoContent();
        }

        // PUT: api/Usuarios/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuarios([FromRoute] int id, [FromBody] Usuarios usuarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != usuarios.Id)
            {
                return BadRequest();
            }

            _context.Entry(usuarios).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Usuarios
        [HttpPost]
        public async Task<IActionResult> PostUsuarios([FromBody] Usuarios usuarios)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Usuarios.Add(usuarios);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUsuarios", new { id = usuarios.Id }, usuarios);
        }

        // DELETE: api/Usuarios/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUsuarios([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var usuarios = await _context.Usuarios.FindAsync(id);
            if (usuarios == null)
            {
                return NotFound();
            }

            _context.Usuarios.Remove(usuarios);
            await _context.SaveChangesAsync();

            return Ok(usuarios);
        }


        private bool UsuariosExists(int id)
        {
            return _context.Usuarios.Any(e => e.Id == id);
        }
    }
}