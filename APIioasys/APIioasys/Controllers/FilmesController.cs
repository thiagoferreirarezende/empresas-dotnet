﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIioasys.Context;
using APIioasys.Models;
using PagedList;
using Microsoft.AspNetCore.Authorization;

namespace APIioasys.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilmesController : ControllerBase
    {
        private readonly DBContext _context;

        public FilmesController(DBContext context)
        {
            _context = context;
        }

        // GET: api/Filmes
        [HttpGet]
        public async Task<List<Filmes>> GetFilmes(int? pagina, string filtro)
        {
            List<Filmes> lstFilme = new List<Filmes>();
            int pageSize = 10;
            int pageNumber = pagina ?? 1;

            lstFilme = _context.Filmes.Where(f => f.Nome == filtro).ToList();
            if (lstFilme.Count <= 0)
            {
                lstFilme = _context.Filmes.Where(f => f.Diretor == filtro).ToList();
                if (lstFilme.Count <= 0)
                {
                    lstFilme = _context.Filmes.Where(f => f.Genero == filtro).ToList();
                    if (lstFilme.Count <= 0)
                    {
                        lstFilme = _context.Filmes.ToList();
                    }
                }
            }
            return lstFilme.OrderBy(v => v.Votos).OrderBy(n => n.Nome).ToPagedList(pageNumber, pageSize).ToList();
        }

        // GET: api/Filmes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFilmes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var filmes = await _context.Filmes.FindAsync(id);

            if (filmes == null)
            {
                return NotFound();
            }

            return Ok(filmes);
        }


        // POST: api/Filmes
        [HttpPost]
        public async Task<IActionResult> PostFilmes([FromBody] Filmes filmes, int usuarioSolicitante)
        {
            List<Atores> lstAtores = new List<Atores>();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var usuarioDB = await _context.Usuarios.FirstOrDefaultAsync(u => u.Id == usuarioSolicitante);

            if (usuarioDB.PerfilId == 2)
            {
                for (int i = 0; i < filmes.Atores.Count; i++)
                {
                    filmes.Atores[i] = await _context.Atores.FirstOrDefaultAsync(a => a.Id == filmes.Atores[i].Id);
                }
                _context.Filmes.Add(filmes);
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetFilmes", new { id = filmes.Id }, filmes);
            }
            else
            {
                return BadRequest();
            }

        }

        private bool FilmesExists(int id)
        {
            return _context.Filmes.Any(e => e.Id == id);
        }
    }
}