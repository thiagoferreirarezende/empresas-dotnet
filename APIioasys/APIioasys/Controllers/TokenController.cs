﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using APIioasys.Context;
using APIioasys.Models;
using APIioasys.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace APIioasys.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly DBContext _context;

        public TokenController(IConfiguration configuration, DBContext context)
        {
            _context = context;
            _configuration = configuration;
        }

        [HttpPost]
        public IActionResult GetToken(string userName, string senha, bool ativo)
        {
            Login login = new Login();
            

            if (login.IsLogin(userName, senha, ativo, _context))
            {
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, userName)
                };
                var key = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(_configuration["SecurityKey"]));

                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: "thiagoFerreiraRezende",
                    audience: "thiagoFerreiraRezende",
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(30),
                    signingCredentials: creds);
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                });
            }
            else
            {
                return BadRequest("Credenciais invalidas...");
            }
        }
    }
}