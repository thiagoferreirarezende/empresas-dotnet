﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APIioasys.Models
{
    public class Filmes
    {
        public Filmes()
        {
            Atores = new List<Atores>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Diretor { get; set; }
        public string Genero { get; set; }
        [Range(0, 4, ErrorMessage = "Votação tem que ser de 0 a 4")]
        public int Votos { get; set; }

        public List<Atores> Atores { get; set; }
    }
}
