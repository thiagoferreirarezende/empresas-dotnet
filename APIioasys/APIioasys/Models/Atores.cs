﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIioasys.Models
{
    public class Atores
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Nome { get; set; }
        [StringLength(1, ErrorMessage ="Sexo tem que ser definido com F ou M")]
        public string Sexo { get; set; }
        public string Nacionalidade { get; set; }

    }
}
