﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace APIioasys.Migrations
{
    public partial class InicialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Filmes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(nullable: true),
                    Diretor = table.Column<string>(nullable: true),
                    Genero = table.Column<string>(nullable: true),
                    Votos = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Filmes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Perfil",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Perfil", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Senha = table.Column<string>(nullable: true),
                    Ativo = table.Column<bool>(nullable: false),
                    PerfilId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Atores",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(nullable: true),
                    Sexo = table.Column<string>(maxLength: 1, nullable: true),
                    Nacionalidade = table.Column<string>(nullable: true),
                    FilmesId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Atores", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Atores_Filmes_FilmesId",
                        column: x => x.FilmesId,
                        principalTable: "Filmes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "Id", "FilmesId", "Nacionalidade", "Nome", "Sexo" },
                values: new object[,]
                {
                    { 1, null, "Americano", "Arnold Tery", "M" },
                    { 13, null, "Italiano", "Astoufo", "M" },
                    { 11, null, "Brasileiro", "Cristovão", "M" },
                    { 10, null, "Americano", "Franciele", "F" },
                    { 9, null, "Americano", "Julio", "M" },
                    { 8, null, "Americano", "Arnold Tery", "M" },
                    { 12, null, "Espanhol", "Chave", "M" },
                    { 6, null, "Italiano", "Arlindo", "M" },
                    { 5, null, "Americano", "Tereza", "F" },
                    { 4, null, "Italiano", "Arnaldo", "M" },
                    { 3, null, "Brasileiro", "Amanda", "F" },
                    { 2, null, "Americano", "Tery", "M" },
                    { 7, null, "Brasileiro", "Fernando", "M" }
                });

            migrationBuilder.InsertData(
                table: "Perfil",
                columns: new[] { "Id", "Descricao" },
                values: new object[,]
                {
                    { 1, "Usuario" },
                    { 2, "Administrador" }
                });

            migrationBuilder.InsertData(
                table: "Usuarios",
                columns: new[] { "Id", "Ativo", "Email", "Nome", "PerfilId", "Senha", "UserName" },
                values: new object[,]
                {
                    { 13, true, "usuario9@gmail.com", "Usuario9", 1, "123", "usuario9" },
                    { 14, true, "usuario10@gmail.com", "Usuario10", 1, "123", "usuario10" },
                    { 15, true, "usuario11@gmail.com", "Usuario11", 1, "123", "usuario11" },
                    { 19, true, "usuario15@gmail.com", "Usuario15", 1, "123", "usuario15" },
                    { 17, true, "usuario13@gmail.com", "Usuario13", 1, "123", "usuario13" },
                    { 18, true, "usuario14@gmail.com", "Usuario14", 1, "123", "usuario14" },
                    { 12, true, "usuario8@gmail.com", "Usuario8", 1, "123", "usuario8" },
                    { 16, true, "usuario12@gmail.com", "Usuario12", 1, "123", "usuario12" },
                    { 11, true, "usuario7@gmail.com", "Usuario7", 1, "123", "usuario7" },
                    { 3, true, "admin3@gmail.com", "Administrador3", 2, "123", "admin3" },
                    { 9, true, "usuario5@gmail.com", "Usuario5", 1, "123", "usuario5" },
                    { 8, true, "usuario4@gmail.com", "Usuario4", 1, "123", "usuario4" },
                    { 7, true, "usuario3@gmail.com", "Usuario3", 1, "123", "usuario3" },
                    { 6, true, "usuario2@gmail.com", "Usuario2", 1, "123", "usuario2" },
                    { 5, true, "usuario1@gmail.com", "Usuario1", 1, "123", "usuario1" },
                    { 4, true, "admin4@gmail.com", "Administrador4", 2, "123", "admin4" },
                    { 20, true, "usuario16@gmail.com", "Usuario16", 1, "123", "usuario16" },
                    { 2, true, "admin2@gmail.com", "Administrador2", 2, "123", "admin2" },
                    { 1, true, "admin1@gmail.com", "Administrador1", 2, "123", "admin1" },
                    { 10, true, "usuario6@gmail.com", "Usuario6", 1, "123", "usuario6" },
                    { 21, true, "admin@gmail.com", "Administrador", 2, "123", "admin" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Atores_FilmesId",
                table: "Atores",
                column: "FilmesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Atores");

            migrationBuilder.DropTable(
                name: "Perfil");

            migrationBuilder.DropTable(
                name: "Usuarios");

            migrationBuilder.DropTable(
                name: "Filmes");
        }
    }
}
