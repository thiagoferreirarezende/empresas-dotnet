﻿using APIioasys.Models;
using APIioasys.Services;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIioasys.Context
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }
        public DbSet<Perfil> Perfil { get; set; }
        public DbSet<Usuarios> Usuarios { get; set; }
        public DbSet<Atores> Atores { get; set; }
        public DbSet<Filmes> Filmes { get; set; }
    }
}
