﻿using APIioasys.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIioasys.Services
{
    public static class ModalBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Perfil>().HasData(
                new Perfil
                {
                    Id = 1,
                    Descricao = "Usuario"
                },
                new Perfil
                {
                    Id = 2,
                    Descricao = "Administrador"
                });
            modelBuilder.Entity<Usuarios>().HasData(
                 new Usuarios
                 {
                     Id = 1,
                     Nome = "Administrador1",
                     Email = "admin1@gmail.com",
                     UserName = "admin1",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 2
                 },
                 new Usuarios
                 {
                     Id = 2,
                     Nome = "Administrador2",
                     Email = "admin2@gmail.com",
                     UserName = "admin2",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 2
                 },
                 new Usuarios
                 {
                     Id = 3,
                     Nome = "Administrador3",
                     Email = "admin3@gmail.com",
                     UserName = "admin3",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 2
                 },
                 new Usuarios
                 {
                     Id = 4,
                     Nome = "Administrador4",
                     Email = "admin4@gmail.com",
                     UserName = "admin4",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 2
                 },
                 new Usuarios
                 {
                     Id = 5,
                     Nome = "Usuario1",
                     Email = "usuario1@gmail.com",
                     UserName = "usuario1",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 6,
                     Nome = "Usuario2",
                     Email = "usuario2@gmail.com",
                     UserName = "usuario2",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 7,
                     Nome = "Usuario3",
                     Email = "usuario3@gmail.com",
                     UserName = "usuario3",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 8,
                     Nome = "Usuario4",
                     Email = "usuario4@gmail.com",
                     UserName = "usuario4",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 9,
                     Nome = "Usuario5",
                     Email = "usuario5@gmail.com",
                     UserName = "usuario5",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 10,
                     Nome = "Usuario6",
                     Email = "usuario6@gmail.com",
                     UserName = "usuario6",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 11,
                     Nome = "Usuario7",
                     Email = "usuario7@gmail.com",
                     UserName = "usuario7",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 12,
                     Nome = "Usuario8",
                     Email = "usuario8@gmail.com",
                     UserName = "usuario8",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 13,
                     Nome = "Usuario9",
                     Email = "usuario9@gmail.com",
                     UserName = "usuario9",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 14,
                     Nome = "Usuario10",
                     Email = "usuario10@gmail.com",
                     UserName = "usuario10",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 15,
                     Nome = "Usuario11",
                     Email = "usuario11@gmail.com",
                     UserName = "usuario11",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 16,
                     Nome = "Usuario12",
                     Email = "usuario12@gmail.com",
                     UserName = "usuario12",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 17,
                     Nome = "Usuario13",
                     Email = "usuario13@gmail.com",
                     UserName = "usuario13",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 18,
                     Nome = "Usuario14",
                     Email = "usuario14@gmail.com",
                     UserName = "usuario14",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 19,
                     Nome = "Usuario15",
                     Email = "usuario15@gmail.com",
                     UserName = "usuario15",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 20,
                     Nome = "Usuario16",
                     Email = "usuario16@gmail.com",
                     UserName = "usuario16",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 1
                 },
                 new Usuarios
                 {
                     Id = 21,
                     Nome = "Administrador",
                     Email = "admin@gmail.com",
                     UserName = "admin",
                     Senha = "123",
                     Ativo = true,
                     PerfilId = 2
                 });
            modelBuilder.Entity<Atores>().HasData(
                 new Atores
                 {
                     Id = 1,
                     Nome = "Arnold Tery",
                    Sexo = "M",
                    Nacionalidade = "Americano"
                 },
                 new Atores
                 {
                     Id = 2,
                     Nome = "Tery",
                     Sexo = "M",
                     Nacionalidade = "Americano"
                 }, new Atores
                 {
                     Id = 3,
                     Nome = "Amanda",
                     Sexo = "F",
                     Nacionalidade = "Brasileiro"
                 }, new Atores
                 {
                     Id = 4,
                     Nome = "Arnaldo",
                     Sexo = "M",
                     Nacionalidade = "Italiano"
                 }, new Atores
                 {
                     Id = 5,
                     Nome = "Tereza",
                     Sexo = "F",
                     Nacionalidade = "Americano"
                 }, new Atores
                 {
                     Id = 6,
                     Nome = "Arlindo",
                     Sexo = "M",
                     Nacionalidade = "Italiano"
                 }, new Atores
                 {
                     Id = 7,
                     Nome = "Fernando",
                     Sexo = "M",
                     Nacionalidade = "Brasileiro"
                 }, new Atores
                 {
                     Id = 8,
                     Nome = "Arnold Tery",
                     Sexo = "M",
                     Nacionalidade = "Americano"
                 }, new Atores
                 {
                     Id = 9,
                     Nome = "Julio",
                     Sexo = "M",
                     Nacionalidade = "Americano"
                 }, new Atores
                 {
                     Id = 10,
                     Nome = "Franciele",
                     Sexo = "F",
                     Nacionalidade = "Americano"
                 }, new Atores
                 {
                     Id = 11,
                     Nome = "Cristovão",
                     Sexo = "M",
                     Nacionalidade = "Brasileiro"
                 }, new Atores
                 {
                     Id = 12,
                     Nome = "Chave",
                     Sexo = "M",
                     Nacionalidade = "Espanhol"
                 }, new Atores
                 {
                     Id = 13,
                     Nome = "Astoufo",
                     Sexo = "M",
                     Nacionalidade = "Italiano"
                 });
        }
    }
}
