﻿using APIioasys.Context;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIioasys.Services
{
    public class Login : ControllerBase
    {

        public bool IsLogin(string userName, string senha, bool ativo, DBContext _context)
        {
            try
            {
                var userDB = _context.Usuarios.FirstOrDefault(u => u.UserName == userName);
                if(userDB != null)
                {
                    return userName.Equals(userDB.UserName) && senha.Equals(userDB.Senha) && userDB.Ativo == true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
